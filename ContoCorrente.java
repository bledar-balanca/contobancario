public class ContoCorrente {
    public double saldo;
    private final boolean bloccato = false;

    ContoCorrente(double saldoIniz) {
        this.saldo = saldoIniz;
    }

    public double getSaldo(){
        if (bloccato) {
            System.out.println("Conto bloccato");
            return 0;
        } else {
            return this.saldo;
        }
    }

    public double bonifico(final double sommaBonifico){
        if (bloccato) {
            System.out.println("Conto bloccato");
            return 0;
        } else {
            this.saldo += sommaBonifico;
            return this.saldo;
        }
    }

    public double pagamento(final double sommaDaPagare){
        if (bloccato) {
            System.out.println("Conto bloccato");
            return 0;
        } else {
            this.saldo = saldo - sommaDaPagare;
            return this.saldo;
        }
    }
}
