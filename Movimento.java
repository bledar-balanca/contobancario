import java.util.Date;

public class Movimento {
    private Date dataRichiesta;
    private Date dataValuta;
    private String descrizione;
    private String tipologia;
    private double importo;

    public Movimento(Date dataRichiesta, Date dataValuta, String descrizione, String tipologia, double importo) {
        this.dataRichiesta = dataRichiesta;
        this.dataValuta = dataValuta;
        this.descrizione = descrizione;
        this.tipologia = tipologia;
        this.importo = importo;
    }


    public Date getDataRichiesta() {
        return dataRichiesta;
    }

    public Date getDataValuta() {
        return dataValuta;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public String getTipologia() {
        return tipologia;
    }

    public double getImporto() {
        return importo;
    }
}

public class ContoCorrente {
    private double saldo;

    public ContoCorrente(double saldoIniziale) {
        saldo = saldoIniziale;
    }

    public void bonifico(Movimento movimento) {
        if (saldo >= movimento.getImporto()) {
            saldo -= movimento.getImporto();
            System.out.println("Bonifico effettuato: " + movimento.getDescrizione());
        } else {
            System.out.println("Saldo insufficiente per effettuare il bonifico.");
        }
    }

    public void prelievo(Movimento movimento) {
        if (saldo >= movimento.getImporto()) {
            saldo -= movimento.getImporto();
            System.out.println("Prelievo effettuato: " + movimento.getDescrizione());
        } else {
            System.out.println("Saldo insufficiente per effettuare il prelievo.");
        }
    }

    public double getSaldo() {
        return saldo;
    }
}
